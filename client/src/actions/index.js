import stream from '../apis/streams'
import { SIGN_IN, SIGN_OUT, CREAT_STREAM } from './type';

export const signIn = (userId) => {
    return {
        type: SIGN_IN,
        payload: userId
    }
}

export const signOut = () => {
    return {
        type: SIGN_OUT,
    }
}

export const creatStream = formValue => async dispatch => {
    const response = await stream.post('/stream', formValue);

    dispatch({
        type: CREAT_STREAM,
        payload: response.data
    });
}