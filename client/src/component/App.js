import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom'
import SteamDelete from './steam/SteamDelete'
import SteamCreate from './steam/SteamCreate'
import SteamShow from './steam/SteamShow'
import SteamList from './steam/SteamList'
import SteamEdit from './steam/SteamEdit'
import Header from './Header'

class App extends Component {
    render() {
        return (
            <div className="ui container">
                <BrowserRouter>
                    <Header/>
                    <div>
                        <Route path='/' exact component={SteamList}/>
                        <Route path='/stream/new'  component={SteamCreate}/>
                        <Route path='/stream/edit'  component={SteamEdit}/>
                        <Route path='/stream/delete'  component={SteamDelete}/>
                        <Route path='/stream/show'  component={SteamShow}/>
                    </div>
                </BrowserRouter>
            </div>
        );
    }
}

export default App;